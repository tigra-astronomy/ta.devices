﻿// This file is part of the TA.Devices project
// 
// File: I2CTransactionException.cs  Created: 2017-07-29@23:01
// Last modified: 2017-07-29@23:08

using System;

namespace TA.Devices
    {
    /// <summary>
    ///     An exception that is thrown when an I2C device transaction fails. The failed transaction is
    ///     available via the
    ///     <see cref="FailedTransaction" /> property.
    /// </summary>
    public class I2CTransactionException : Exception
        {
        /// <summary>
        ///     Initializes a new instance of the <see cref="I2CTransactionException" /> class.
        /// </summary>
        /// <param name="transaction">The failed transaction.</param>
        /// <param name="message">A message that describes the reason for the failure (optional).</param>
        /// <param name="inner">Any inner exception (optional).</param>
        public I2CTransactionException(
            I2CCompletedDeviceTransaction transaction, string message = "Transaction failed",
            Exception inner = null) : base(message, inner)
            {
            FailedTransaction = transaction;
            }

        public I2CCompletedDeviceTransaction FailedTransaction { get; }
        }
    }