﻿// This file is part of the TA.Devices project
// 
// File: ChecksumStatus.cs  Created: 2017-07-29@23:01
// Last modified: 2017-07-29@23:04

namespace TA.Devices
    {
    public enum ChecksumStatus
        {
        NotChecked,
        Valid,
        Invalid
        }
    }