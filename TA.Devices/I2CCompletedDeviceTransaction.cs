﻿// This file is part of the TA.Devices project
// 
// File: I2CCompletedDeviceTransaction.cs  Created: 2017-07-29@23:01
// Last modified: 2017-07-29@23:08

using Windows.Devices.I2c;
using TA.IoT;

namespace TA.Devices
    {
    /// <summary>
    ///     Represents a completed transaction with an I2C device.
    /// </summary>
    public sealed class I2CCompletedDeviceTransaction : DeviceTransaction
        {
        internal I2CCompletedDeviceTransaction(
            IDeviceTransaction source, II2CDevice device, I2cTransferResult result,
            ChecksumStatus check = ChecksumStatus.NotChecked) : base(source.TransmitBuffer, source.ReceiveBuffer)
            {
            Device = device;
            Result = result;
            PecStatus = check;
            }

        /// <summary>
        ///     Gets the device with which the transaction was conducted.
        /// </summary>
        /// <value>The device.</value>
        public II2CDevice Device { get; }

        /// <summary>
        ///     Gets the Packet Error Check (CRC) status.
        /// </summary>
        /// <value>The pec status.</value>
        public ChecksumStatus PecStatus { get; }

        /// <summary>
        ///     Gets the result of the transaction.
        /// </summary>
        /// <value>The result.</value>
        public I2cTransferResult Result { get; }
        }
    }