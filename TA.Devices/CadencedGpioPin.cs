﻿// This file is part of the TA.Devices project
// 
// File: CadencedGpioPin.cs  Created: 2017-07-29@01:20
// Last modified: 2017-07-29@02:16

using System;
using Windows.Devices.Gpio;
using TA.IoT;

namespace TA.Devices
    {
    internal class CadencedGpioPin
        {
        private static SynchronizedCadenceGenerator generator;
        private readonly GpioPin pin;
        private string myId;

        public CadencedGpioPin(GpioPin pin, TimeSpan? updatePeriod = null)
            {
            this.pin = pin;
            if (generator == null)
                generator = new SynchronizedCadenceGenerator(updatePeriod ?? TimeSpan.FromMilliseconds(50));
            myId = generator.Add(TurnOn, TurnOff, 0xAAAAAAAAAAAAAAAA, 0x5555555555555555, false,
                $"GPIO Pin {pin.PinNumber}");
            }

        public void SetCadence(ulong pattern)
            {
            // ToDo generator.SetCadence(myId, pattern);
            }

        private void TurnOff() => pin.Write(GpioPinValue.Low);

        private void TurnOn() => pin.Write(GpioPinValue.High);
        }
    }