﻿// This file is part of the TA.Devices project
// 
// File: TaskExtensions.cs  Created: 2017-07-30@01:54
// Last modified: 2017-07-30@01:56

using System.Threading.Tasks;

namespace TA.Devices.ExtensionMethods
    {
    public static class TaskExtensions
        {
        public static TResult WaitForResult<TResult>(this Task<TResult> asyncOperation)
            {
            asyncOperation.Wait();
            return asyncOperation.Result;
            }
        }
    }