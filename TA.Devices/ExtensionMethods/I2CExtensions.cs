﻿// This file is part of the TA.Devices project
// 
// File: I2CExtensions.cs  Created: 2017-07-29@23:37
// Last modified: 2017-07-29@23:42

using System;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Threading.Tasks;
using Windows.Devices.Enumeration;
using Windows.Devices.I2c;
using TA.IoT;
using TA.IoT.Builders;

namespace TA.Devices.ExtensionMethods
    {
    public static class I2CExtensions
        {
        public static async Task<II2CDevice> GetDefaultI2CDeviceAsync(this I2cConnectionSettings settings)
            {
            var controller = await I2cController.GetDefaultAsync();
            var nativeDevice = controller.GetDevice(settings);
            var betterImplementation = new I2CDevice(nativeDevice);
            return betterImplementation;
            }

        /// <summary>
        ///     Gets the I2C controllers on the system
        /// </summary>
        /// <param name="address">
        ///     The 7-bit I2C slave address of the target device. Don't get clever and try to allow for
        ///     the R/W bit; I2C addresses should be in the range 0x00 to 0x7F. The PCF8591 accepts
        ///     addresses in the format <c>1 0 0 1 x x x</c>where bits 2, 1 and 0 are set by the
        ///     hardware implementation (sometimes these are configurable by jumpers or solder pads on
        ///     the PCB). This leaves a possible range of addresses from 0x48 to 0x4F, with the default
        ///     being 0x48 is not specified.
        /// </param>
        /// <returns>A configured <see cref="I2cDevice" />.</returns>
        public static async Task<DeviceInformationCollection> GetI2CControllers()
            {
            var aqs = I2cDevice.GetDeviceSelector();
            return await DeviceInformation.FindAllAsync(aqs);
            }

        private static ChecksumStatus GetPecStatus(IDeviceTransaction transaction, int address, Crc8 validator)
            {
            var slaveAddress = (byte) (address << 1);
            byte[] writeAddress = {slaveAddress};
            byte[] readAddress = {(byte) (slaveAddress + 1)};
            var bytesToValidate = Enumerable.Empty<byte>();
            if (transaction.TransmitBuffer.Any())
                bytesToValidate = bytesToValidate.Concat(writeAddress).Concat(transaction.TransmitBuffer);
            if (transaction.ReceiveBuffer.Any())
                bytesToValidate = bytesToValidate.Concat(readAddress).Concat(transaction.ReceiveBuffer);
            var status = validator.IsValid(bytesToValidate.ToArray());
            return status ? ChecksumStatus.Valid : ChecksumStatus.Invalid;
            }

        public static bool IsPacketChecksumValid(this IDeviceTransaction transaction, Crc8 crc)
            {
            Contract.Requires(transaction != null);
            Contract.Requires(crc != null);
            return crc.IsValid(transaction.TransmitBuffer.Concat(transaction.ReceiveBuffer).ToArray());
            }

        /// <summary>
        ///     Conducts a transaction with the device.
        /// </summary>
        /// <param name="device">The device.</param>
        /// <param name="builder">A transaction builder.</param>
        /// <param name="validator">
        ///     The validator that will be used to validate the Packet Error Code (or null to skip
        ///     validation).
        /// </param>
        /// <returns>
        ///     An <see cref="I2CCompletedDeviceTransaction" /> that contains the results and state information
        ///     about the completed transaction.
        /// </returns>
        public static I2CCompletedDeviceTransaction Transact(
            this II2CDevice device, ITransactionBuilder builder, Crc8 validator = null)
            {
            Contract.Requires(device != null);
            Contract.Requires(builder != null);
            return device.Transact(builder.Build(), validator);
            }

        /// <summary>
        ///     Conducts a transaction with the device.
        /// </summary>
        /// <param name="device">The device.</param>
        /// <param name="transaction">The transaction.</param>
        /// <returns>An <see cref="I2cTransferResult" /> indicating the disposition of the transaction.</returns>
        public static I2CCompletedDeviceTransaction Transact(
            this II2CDevice device, IDeviceTransaction transaction, Crc8 validator = null)
            {
            Contract.Requires(device != null);
            Contract.Requires(transaction != null);
            I2cTransferResult result;
            if (transaction.TransmitBuffer.Any())
                {
                if (transaction.ReceiveBuffer.Any())
                    result = device.WriteReadPartial(transaction.TransmitBuffer, transaction.ReceiveBuffer);
                else
                    result = device.WritePartial(transaction.TransmitBuffer);
                }
            else
                result = device.ReadPartial(transaction.ReceiveBuffer);

            ChecksumStatus pecStatus;
            if (result.Status != I2cTransferStatus.FullTransfer || validator == null)
                pecStatus = ChecksumStatus.NotChecked;
            else
                pecStatus = GetPecStatus(transaction, device.ConnectionSettings.SlaveAddress, validator);
            return new I2CCompletedDeviceTransaction(transaction, device, result, pecStatus);
            }
        }
    }