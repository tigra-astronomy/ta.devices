﻿// This file is part of the TA.UWP.IoTUtilities project
// 
// Copyright © 2015-2015 Tigra Astronomy., all rights reserved.
// 
// File: PlatformInfo.cs  Last modified: 2015-11-10@15:27 by Tim Long

using Windows.ApplicationModel;
using Windows.Security.ExchangeActiveSyncProvisioning;
using Windows.System.Profile;

namespace TA.Devices
{
    /// <summary>
    ///     An immutable type that provides runtime information about the target platform that the code is running on.
    /// </summary>
    public sealed class PlatformInfo
    {
        private PlatformInfo()
        {
            // get the system family name
            var ai = AnalyticsInfo.VersionInfo;
            SystemFamily = ai.DeviceFamily;

            // get the system version number
            var sv = AnalyticsInfo.VersionInfo.DeviceFamilyVersion;
            var v = ulong.Parse(sv);
            var v1 = (v & 0xFFFF000000000000L) >> 48;
            var v2 = (v & 0x0000FFFF00000000L) >> 32;
            var v3 = (v & 0x00000000FFFF0000L) >> 16;
            var v4 = v & 0x000000000000FFFFL;
            SystemVersion = $"{v1}.{v2}.{v3}.{v4}";

            // get the package architecure
            var package = Package.Current;
            SystemArchitecture = package.Id.Architecture.ToString();

            // get the user friendly app name
            ApplicationName = package.DisplayName;

            // get the app version
            var pv = package.Id.Version;
            ApplicationVersion = $"{pv.Major}.{pv.Minor}.{pv.Build}.{pv.Revision}";

            // get the device manufacturer and model name
            var eas = new EasClientDeviceInformation();
            DeviceManufacturer = eas.SystemManufacturer;
            DeviceModel = eas.SystemProductName;
        }

        public string SystemFamily { get; }
        public string SystemVersion { get; }
        public string SystemArchitecture { get; }
        public string ApplicationName { get; }
        public string ApplicationVersion { get; }
        public string DeviceManufacturer { get; }
        public string DeviceModel { get; }

        /// <summary>
        ///     Reads the platform information and returns an immutable instance of <see cref="PlatformInfo" /> populated
        ///     with the current values.
        /// </summary>
        /// <returns>PlatformInfo.</returns>
        public static PlatformInfo GetPlaformInfo()
        {
            return new PlatformInfo();
        }
    }
}