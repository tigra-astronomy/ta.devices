// This file is part of the TA.Devices project
// 
// File: II2cDevice.cs  Created: 2017-07-29@23:01
// Last modified: 2017-07-29@23:08

using Windows.Devices.I2c;

namespace TA.Devices
    {
    public interface II2CDevice
        {
        I2cConnectionSettings ConnectionSettings { get; }

        string DeviceId { get; }

        void Read(byte[] buffer);

        I2cTransferResult ReadPartial(byte[] buffer);

        void Write(byte[] buffer);

        I2cTransferResult WritePartial(byte[] buffer);

        void WriteRead(byte[] writeBuffer, byte[] readBuffer);

        I2cTransferResult WriteReadPartial(byte[] writeBuffer, byte[] readBuffer);
        }
    }