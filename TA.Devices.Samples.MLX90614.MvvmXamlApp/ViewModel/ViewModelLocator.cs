﻿// This file is part of the TA.Devices project
// 
// File: ViewModelLocator.cs  Created: 2017-07-30@01:48
// Last modified: 2017-08-01@20:26

using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Views;
using Microsoft.Practices.ServiceLocation;
using TA.Devices.ExtensionMethods;
using TA.Devices.Samples.MLX90614.MvvmXamlApp.Model;

namespace TA.Devices.Samples.MLX90614.MvvmXamlApp.ViewModel
    {
    public class ViewModelLocator
        {
        private static II2CDevice i2cController;

        static ViewModelLocator()
            {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            var nav = new NavigationService();
            SimpleIoc.Default.Register<INavigationService>(() => nav);

            var task = CreateDataService();
            task.Wait();
            var dataService = task.Result;
            SimpleIoc.Default.Register(() => dataService);
            SimpleIoc.Default.Register<TemperatureSensorViewModel>();
            }


        [SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public TemperatureSensorViewModel MainPageViewModel
            => ServiceLocator.Current.GetInstance<TemperatureSensorViewModel>();

        private static async Task<ITemperatureSensorDataService> CreateDataService()
            {
            if (ViewModelBase.IsInDesignModeStatic)
                return new DesignTimeTemperatureSensorDataService("Simulated data - Design mode");
            string sourceDescription;
            try
                {
                sourceDescription = CreateI2cController();
                }
            catch (Exception e)
                {
                i2cController = null;
                sourceDescription = "Simulated data - no I2C controller";
                }

            if (i2cController == null)
                return new DesignTimeTemperatureSensorDataService(sourceDescription);

            var sensor = new MLX90614Thermometer(i2cController) {Offset = -273.15, ValueChangedThreshold = 0.1};
            sensor.Reset(); // ToDo - can this be made async?
            var dataService = new TemperatureSensorDataService(sensor, sourceDescription);
            await Task.CompletedTask; // This is really here to stop a compiler warning
            return dataService;
            }

        private static string CreateI2cController()
            {
            string sourceDescription; // Try to create a GPIO controller, and if it fails, use simulated data.
            // Set the Lightning Provider as the default if Lightning driver is enabled on the target device
            // Otherwise, the inbox provider will continue to be the default
            /** Lightning provider not supported yet on build 15063 Creator's Update
            if (LightningProvider.IsLightningEnabled)
                {
                // Set Lightning as the default provider
                LowLevelDevicesController.DefaultProvider = LightningProvider.GetAggregateProvider();
                sourceDescription = "Live data - GPIO Using Lightning Provider";
                }
            else
    */
            sourceDescription = "Live data - GPIO Using Inbox Provider";

            var connectionSettings = MLX90614Thermometer.ConnectionSettings();
            i2cController = connectionSettings.GetDefaultI2CDeviceAsync().Result;
            if (i2cController == null)
                sourceDescription = "no GPIO controller - simulated data";
            return sourceDescription;
            }
        }
    }