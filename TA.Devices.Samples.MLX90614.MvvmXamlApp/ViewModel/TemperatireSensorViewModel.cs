﻿// This file is part of the TA.Devices project
// 
// File: TemperatireSensorViewModel.cs  Created: 2017-07-30@01:48
// Last modified: 2017-07-31@22:11

using System.Threading.Tasks;
using Windows.UI.Xaml;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Threading;
using GalaSoft.MvvmLight.Views;
using TA.Devices.Samples.MLX90614.MvvmXamlApp.Model;
using TA.IoT;

namespace TA.Devices.Samples.MLX90614.MvvmXamlApp.ViewModel
    {
    public class TemperatureSensorViewModel : ViewModelBase
        {
        private const double AbsoluteZero = -273.15;
        private readonly ITemperatureSensorDataService dataService;
        private readonly INavigationService navigationService;
        private double ambientTemperature;
        private double channel1Temperature;
        private double channel2Temperature;
        private float emissivity = 1.0f;
        private RelayCommand exitCommand;
        private RelayCommand setEmissivityCommand;

        private RelayCommand startAutoSamplingCommand;
        private RelayCommand stopAutoSamplingCommand;
        private RelayCommand takeSampleCommand;

        public TemperatureSensorViewModel(
            ITemperatureSensorDataService dataService, INavigationService navigationService)
            {
            this.dataService = dataService;
            this.navigationService = navigationService;
            Initialize();
            }

        public double Ambient
            {
            get => ambientTemperature;
            private set
                {
                ambientTemperature = value;
                DispatcherHelper.CheckBeginInvokeOnUI(() => RaisePropertyChanged());
                }
            }

        public double Channel1
            {
            get => channel1Temperature;
            private set
                {
                channel1Temperature = value;
                DispatcherHelper.CheckBeginInvokeOnUI(() => RaisePropertyChanged());
                }
            }

        public double Channel2
            {
            get => channel2Temperature;
            private set
                {
                channel2Temperature = value;
                DispatcherHelper.CheckBeginInvokeOnUI(() => RaisePropertyChanged());
                }
            }

        public string DataSourceDescription => dataService.DataSourceDescription;

        public float Emissivity
            {
            get => emissivity;
            set
                {
                emissivity = value;
                RaisePropertyChanged();
                }
            }

        public RelayCommand ExitCommand => exitCommand ?? (exitCommand = new RelayCommand(Exit, CanExit));

        public RelayCommand SetEmissivityCommand => setEmissivityCommand ??
                                                    (setEmissivityCommand = new RelayCommand(SetEmissivity));

        /*
         * Command Properties
         */

        public RelayCommand StartAutoSamplingCommand
            =>
                startAutoSamplingCommand
                ?? (startAutoSamplingCommand = new RelayCommand(StartAutoSampling, CanStartAutoSampling));

        public RelayCommand StopAutoSamplingCommand
            =>
                stopAutoSamplingCommand
                ?? (stopAutoSamplingCommand = new RelayCommand(StopAutoSampling, CanStopAutoSampling));

        public RelayCommand TakeSampleCommand
            =>
                takeSampleCommand
                ?? (takeSampleCommand = new RelayCommand(async () => await TakeSensorSample(), CanTakeManualSample));

        private bool CanExit() => true;

        private bool CanStartAutoSampling() => !dataService.PeriodicSamplingActive;

        private bool CanStopAutoSampling() => dataService.PeriodicSamplingActive;

        private bool CanTakeManualSample() => !dataService.PeriodicSamplingActive;

        private void Exit()
            {
            Application.Current.Exit();
            }

        private void HandleSensorValueChanged(object sender, SensorValueChangedEventArgs e)
            {
            switch (e.Channel)
                {
                    case 0:
                        Ambient = e.Value;
                        break;
                    case 1:
                        Channel1 = e.Value;
                        break;
                    case 2:
                        Channel2 = e.Value;
                        break;
                }
            }

        private void Initialize()
            {
            //await TakeSensorSample();
            dataService.Reset();
            }

        private void SetEmissivity()
            {
            dataService.SetEmissivity(emissivity);
            }

        private void StartAutoSampling()
            {
            dataService.ValueChanged += HandleSensorValueChanged;
            dataService.StartPeriodicSampling(Timeout.FromMilliseconds(200));
            UpdateUiState();
            }

        private void StopAutoSampling()
            {
            dataService.StopPeriodicSampling();
            dataService.ValueChanged -= HandleSensorValueChanged;
            UpdateUiState();
            }

        private async Task TakeSensorSample()
            {
            await dataService.SampleAllChannelsAsync();
            Ambient = dataService[0];
            Channel1 = dataService[1];
            Channel2 = dataService[2];
            }


        /// <summary>
        ///     Updates the state of the UI by raising *CanExecuteChanged events.
        /// </summary>
        private void UpdateUiState()
            {
            StartAutoSamplingCommand.RaiseCanExecuteChanged();
            StopAutoSamplingCommand.RaiseCanExecuteChanged();
            TakeSampleCommand.RaiseCanExecuteChanged();
            ExitCommand.RaiseCanExecuteChanged();
            }
        }
    }