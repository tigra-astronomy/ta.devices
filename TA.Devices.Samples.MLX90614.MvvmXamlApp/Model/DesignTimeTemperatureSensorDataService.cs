﻿// This file is part of the TA.Devices project
// 
// File: DesignTimeTemperatureSensorDataService.cs  Created: 2017-07-30@00:55
// Last modified: 2017-07-30@01:51

using System;
using System.Threading.Tasks;
using TA.IoT;

namespace TA.Devices.Samples.MLX90614.MvvmXamlApp.Model
    {
    public class DesignTimeTemperatureSensorDataService : ITemperatureSensorDataService
        {
        private readonly Random random = new Random();

        public DesignTimeTemperatureSensorDataService(string description)
            {
            DataSourceDescription = description;
            }

        public string DataSourceDescription { get; }

        /// <summary>
        ///     Gets the value of the specified A-to-D channel.
        /// </summary>
        /// <param name="channel">The channel to be read.</param>
        /// <returns>
        ///     The scaled value of the channel.
        /// </returns>
        public double this[uint channel] => random.NextDouble() * 273.15 * 2 - 273.15;

        public void Reset() { }

        public Task SampleAllChannelsAsync() => Task.CompletedTask;

        #region Implementation of INotifySensorValueChanged
        /// <summary>
        ///     Gets or sets the amount that a channel's value must change by in order to trigger the
        ///     <see cref="INotifySensorValueChanged.ValueChanged" /> event. The default value for this property is
        ///     implementation specific and will typically be related to the precision of the sensor.
        /// </summary>
        /// <value>The threshold.</value>
        public double ValueChangedThreshold { get; set; }

        event EventHandler<SensorValueChangedEventArgs> INotifySensorValueChanged.ValueChanged
            {
            add => throw new NotImplementedException();
            remove => throw new NotImplementedException();
            }

#pragma warning disable 0067    // Event is never raised at design-time but is required by an interface
        /// <summary>
        ///     Occurs when the sensor value has changed by more than <see cref="ValueChangedThreshold" />.
        /// </summary>
        public event EventHandler<SensorValueChangedEventArgs> ValueChanged;
#pragma warning restore 0067
        #endregion

        #region Implementation of IPeriodicSampling
        /// <summary>
        ///     Starts sampling the A-to-D channels repeatedly, at the specified interval. Updated samples will be
        ///     reflected in the channel values and the device will raise <see cref="ValueChanged" /> events in response
        ///     to the reading changing beyond the <see cref="ValueChangedThreshold" />.
        /// </summary>
        /// <param name="interval">The interval between samples.</param>
        public void StartPeriodicSampling(Timeout interval)
            {
            PeriodicSamplingActive = true;
            }

        void IPeriodicSampling.StartPeriodicSampling(Timeout interval)
            {
            StartPeriodicSampling(interval);
            }

        /// <summary>
        ///     Stops periodic sampling.
        /// </summary>
        public void StopPeriodicSampling()
            {
            PeriodicSamplingActive = false;
            }

        public void SetEmissivity(float emissivityFactor) { }

        /// <summary>
        ///     Gets a value indicating whether periodic sampling is active.
        /// </summary>
        /// <value><c>true</c> if periodic sampling is active; otherwise, <c>false</c>.</value>
        public bool PeriodicSamplingActive { get; private set; }
        #endregion
        }
    }