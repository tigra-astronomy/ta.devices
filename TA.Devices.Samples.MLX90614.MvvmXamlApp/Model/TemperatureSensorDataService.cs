﻿// This file is part of the TA.Devices project
// 
// File: TemperatureSensorDataService.cs  Created: 2017-07-29@23:50
// Last modified: 2017-07-30@00:27

using System;
using System.Threading.Tasks;
using TA.IoT;

namespace TA.Devices.Samples.MLX90614.MvvmXamlApp.Model
    {
    public class TemperatureSensorDataService : ITemperatureSensorDataService
        {
        private readonly MLX90614Thermometer sensor;

        /// <summary>
        ///     Initializes a new instance of the <see cref="TemperatureSensorDataService" /> class.
        /// </summary>
        /// <param name="sensor">The sensor.</param>
        /// <param name="sourceDescription"></param>
        public TemperatureSensorDataService(MLX90614Thermometer sensor, string sourceDescription)
            {
            this.sensor = sensor;
            DataSourceDescription = sourceDescription;
            }

        /// <summary>
        ///     Gets the precision of the thermometer in Kelvin; that is, the smallest amount by which two
        ///     consecutive readings can differ. This implementation uses a moving average so the difference between two
        ///     average values can actually be smaller than this, however the true precision is directly determined by
        ///     the bit width of the underlying device.
        /// </summary>
        /// <value>The maximum value.</value>
        public double Precision => sensor.Precision;

        public event EventHandler<SensorValueChangedEventArgs> ValueChanged
            {
            add => sensor.ValueChanged += value;
            remove => sensor.ValueChanged -= value;
            }

        /// <summary>
        ///     Gets or sets the amount that a channel's value must change by in order to trigger the
        ///     <see cref="MLX90614Thermometer.ValueChanged" />
        ///     event.
        ///     The default value for this property is determined by the device's precision and is implementation specific.
        /// </summary>
        /// <value>The threshold.</value>
        public double ValueChangedThreshold
            {
            get => sensor.ValueChangedThreshold;
            set => sensor.ValueChangedThreshold = value;
            }

        /// <summary>
        ///     Gets a value indicating whether periodic sampling is active.
        /// </summary>
        /// <value><c>true</c> if periodic sampling is active; otherwise, <c>false</c>.</value>
        public bool PeriodicSamplingActive => sensor.PeriodicSamplingActive;

        /// <summary>
        ///     Starts automatic periodic sampling. <see cref="MLX90614Thermometer.ValueChanged" /> events will be raised whenever
        ///     a channel's value
        ///     changes.
        /// </summary>
        /// <param name="interval">The interval.</param>
        public void StartPeriodicSampling(Timeout interval)
            {
            sensor.StartPeriodicSampling(interval);
            }

        /// <summary>
        ///     Stops automatic periodic sampling.
        /// </summary>
        public void StopPeriodicSampling()
            {
            sensor.StopPeriodicSampling();
            }

        public string DataSourceDescription { get; }

        /// <summary>
        ///     Gets or clears the moving average value of the specified channel.
        /// </summary>
        /// <param name="channel">The channel to be read.</param>
        /// <returns>The moving average value of the channel taken over the population size specified in the constructor.</returns>
        public double this[uint channel] => sensor[channel].LastValue;

        public Task SampleAllChannelsAsync() => sensor.UpdateAllChannelsAsync();

        public void SetEmissivity(float emissivityFactor) => sensor.SetEmissivity(emissivityFactor);

        /// <summary>
        ///     Resets and reinitializes any hardware devices and clears all readings to zero.
        /// </summary>
        /// <returns>Task.</returns>
        public void Reset() => sensor.Reset();
        }
    }