﻿// This file is part of the TA.Devices project
// 
// File: ITemperatureSensorDataService.cs  Created: 2017-07-29@23:50
// Last modified: 2017-07-30@00:12

using System.Threading.Tasks;
using TA.IoT;

namespace TA.Devices.Samples.MLX90614.MvvmXamlApp.Model
    {
    public interface ITemperatureSensorDataService : INotifySensorValueChanged, IPeriodicSampling
        {
        string DataSourceDescription { get; }

        void SetEmissivity(float emissivityFactor);

        double this[uint channel] { get; }

        Task SampleAllChannelsAsync();

        void Reset();
        }
    }